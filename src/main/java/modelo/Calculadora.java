/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Pablo
 */
public class Calculadora {

    /**
     * @return the n3
     */
    private int n1;
    private int n2;
    private int n3;
    private int operacion;

    /**
     * @return the operacion
     */
    public int getoperacion() {
        return operacion;
    }

    /**
     * @param operacion the operacion to set
     */
    public void setoperacion(int operacion) {
        this.operacion = operacion;
    }

    /**
     * @return the n1
     */
    public int getN1() {
        return n1;
    }

    /**
     * @param n1 the n1 to set
     */
    public void setN1(int n1) {
        this.n1 = n1;
    }

    /**
     * @return the n2
     */
    public int getN2() {
        return n2;
    }

    /**
     * @param n2 the n2 to set
     */
    public void setN2(int n2) {
        this.n2 = n2;
    }

        public int getN3() {
        return n3;
    }

    /**
     * @param n3 the n3 to set
     */
    public void setN3(int n3) {
        this.n3 = n3;
    }

    public int operacion() {

        //si los valores son mayores a 1000 entonces rechazar
        
        return (int) (this.getN1() * this.getN3()/100 * this.getN2());

    }

    public int operacion(int n1, int n2, int n3) {
        this.setN1(n1);
        this.setN2(n2);
        this.setN3(n3);

        return operacion();

    }

    
}
